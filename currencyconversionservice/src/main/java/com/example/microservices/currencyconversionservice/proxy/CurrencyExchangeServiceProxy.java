package com.example.microservices.currencyconversionservice.proxy;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import com.example.microservices.currencyconversionservice.vo.CurrencyConversionBean;
import com.example.microservices.currencyconversionservice.vo.ExchangeValue;

@FeignClient(name = "currency-exchange-service")
public interface CurrencyExchangeServiceProxy {

	@PostMapping("/currencyexchange/from/{from}/to/{to}")
	public ResponseEntity<ExchangeValue> retrieveExchangeValue(
			@PathVariable("from") String from, @PathVariable("to") String to);

}
