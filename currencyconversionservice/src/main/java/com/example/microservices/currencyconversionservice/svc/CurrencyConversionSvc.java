package com.example.microservices.currencyconversionservice.svc;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.microservices.currencyconversionservice.proxy.CurrencyExchangeServiceProxy;
import com.example.microservices.currencyconversionservice.vo.CurrencyConversionBean;
import com.example.microservices.currencyconversionservice.vo.ExchangeValue;

@RestController
public class CurrencyConversionSvc {
	@Autowired
	private CurrencyExchangeServiceProxy currencyExchangeServiceProxy;
	private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyConversionSvc.class);

	@PostMapping("/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConversionBean convertCurrency(@PathVariable String from, @PathVariable String to,
			@PathVariable BigDecimal quantity) {
		LOGGER.info("convertCurrency");
//		ResponseEntity<ExchangeValue> bean = currencyExchangeServiceProxy
//				.retrieveExchangeValue(from, to);
		ResponseEntity<ExchangeValue> bean = currencyExchangeServiceProxy
				.retrieveExchangeValue(from, to);
		return new CurrencyConversionBean(bean.getBody().getId(), from, to, bean.getBody().getConversionMutiple(),
				quantity, quantity.multiply(bean.getBody().getConversionMutiple()), bean.getBody().getPort());
	}

}
